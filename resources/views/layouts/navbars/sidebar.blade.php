<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner scroll-scrollx_visible">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{ route('home') }}">
                <h1 class=" text-purple">Tradnios</h1>
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item active">
                    <li class="nav-item {{ $parentSection ?? '' == 'task' ? 'active' : '' }}">
                        <a class="nav-link" href="#navbar-task" data-toggle="collapse" role="button"
                           aria-expanded="{{ $parentSection ?? '' == 'task' ? 'true' : '' }}"
                           aria-controls="navbar-pages">
                            <i class="ni ni-ui-04 text-info"></i>
                            <span class="nav-link-text">{{ __('Task') }}</span>
                        </a>
                        <div class="collapse {{ $parentSection ?? '' == 'task' ? 'show' : '' }}" id="navbar-task">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{ $elementName ?? '' == 'tasks' ? 'active' : '' }}">
                                    <a href="{{ route('task.index', 'task') }}"
                                       class="nav-link">{{ __('Task Management') }}</a>
                                </li>
                            </ul>
                        </div>

                    </li>
                    @role('super_admin')
                    <li class="nav-item {{ $parentSection ?? '' == 'category' ? 'active' : '' }}">
                        <a class="nav-link" href="#navbar-category" data-toggle="collapse" role="button"
                           aria-expanded="{{ $parentSection ?? '' == 'category' ? 'true' : '' }}"
                           aria-controls="navbar-pages">
                            <i class="ni ni-collection text-yellow"></i>
                            <span class="nav-link-text">{{ __('Category') }}</span>
                        </a>
                        <div class="collapse {{ $parentSection ?? '' == 'category' ? 'show' : '' }}"
                             id="navbar-category">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{ $elementName ?? '' == 'categorys' ? 'active' : '' }}">
                                    <a href="{{ route('category.index', 'category') }}"
                                       class="nav-link">{{ __('Category Management') }}</a>
                                </li>
                            </ul>
                        </div>
                        @endrole
                    </li>


                </ul>
            </div>
        </div>
    </div>
</nav>
