@extends('layouts.app', [
    'title' => __($item.' Management'),
    'parentSection' => 'laravel',
    'elementName' => $item.'-management'
])

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"></script>

@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __($item) }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route($item.'.index') }}">{{ __($item.' Management') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Edit '.$item) }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __($item.' Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route($item.'.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route($item.'.update',$task) }}" autocomplete="off">
                            @csrf
                            @method('PUT')
                            <h6 class="heading-small text-muted mb-4">{{ __($item.' information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Name') }}" value="{{ old('name',$task->name) }}" required autofocus>

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Description') }}</label>
                                    <textarea class="form-control" name="description" id="description" rows="3"
                                              placeholder="{{ __('Description') }}" required >{{$task->description}}</textarea>
                                    @include('alerts.feedback', ['field' => 'description'])
                                </div>
                                <div class="form-group">
                                    <label for="user_id"><span class="text-danger"></span></label>
                                    <select class="form-control m-bot15" name="user_id" id="user_id">
                                        @foreach($users as $user)
                                            <option
                                                value="{{$user->id}}" {{array_search($user->id,$users->pluck('id','id')->toArray()) ? 'selected' :''}}>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    @include('alerts.feedback', ['field' => 'user_id'])
                                </div>
                                <div class="form-group{{ $errors->has('create_date') ? ' has-danger' : '' }}">
                                    <label class="form-control-label">{{__('Create Date')}}</label>
                                    <input class="form-control" name="create_date" type="datetime-local" value="{{old('create_date',$task->create_date->format('Y-m-d')."T".$task->create_date->format('H:i'))}}"
                                           id="example-datetime-local-input">
                                    @include('alerts.feedback', ['field' => 'create_date'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{__('Dead Line')}}</label>
                                    <input class="form-control" name="dead_line" type="date" value="{{old('dead_line',$task->dead_line->format('Y-m-d'))}}"
                                           id="example-date-input">
                                    @include('alerts.feedback', ['field' => 'dead_line'])

                                </div>

                                <div class="form-group" id="permission-wrapper">
                                    <label for="name">{{__('Categories')}}<span
                                            class="text-danger"></span></label>
                                    <div class="parsley-checkbox" id="cbWrapper">
                                        <div class="row">
                                            @foreach($categories as $category)
                                                <div class="col-4 mb-3">
                                                    <label class="ckbox">
                                                        <input
                                                            name="categories[]" type="checkbox"
                                                            value="{{$category->id}}" {{array_key_exists($category->id, $task->categories->pluck('id','id')->toArray()) ? ' checked' : ''}}>
                                                        <span class="badge badge-default"
                                                              style="background-color:{{ $category->color }}">{{ $category->name }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">{{'Sub Task'}}</label>
                                        <div class="card mt-3">
                                            <div class="card-body">
                                                <table class="table table-bordered" id="dynamicAddRemove">
                                                    <tr>
                                                        <th>Title</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    <tr>
                                                        <td><input id="content" type="text" placeholder="Enter title"
                                                                   class="form-control"/></td>
                                                        <td>
                                                            <button type="button" name="add" id="add-btn"
                                                                    class="btn btn-success">Add Sub Task
                                                            </button>
                                                        </td>
                                                        @foreach($task->subTasks as $subTask)
                                                        <tr><td><input type="text" id="subTask-' + i + '" value="{{old('content',$subTask->content)}}" name="subTask[{{$subTask->id}}]"  placeholder="Enter title" class="form-control" required /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>
                                                        @endforeach
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <script src="{{asset('argon/js/dynamic.js')}}"></script>
                </div>
            </div>
        </div>
    </div>
        @include('layouts.footers.auth')
    </div>
@endsection
