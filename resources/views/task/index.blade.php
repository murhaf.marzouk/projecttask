@extends('layouts.app', [
    'title' => __($item.' Management'),
    'parentSection' => 'laravel',
    'elementName' => $item.'-management'
])

@section('content')
    @component('layouts.headers.auth')
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __($item) }}</h3>
                            </div>
                            @role('super_admin')
                            <div class="col-4 text-right">
                                <a href="{{ route($item.'.create') }}"
                                   class="btn btn-sm btn-primary">{{ __('Add '.$item) }}</a>
                            </div>
                            @endrole
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <form class="pl-3" action="{{route($item.'.index')}}" method="get">
                        <div class="row">
                            @role('super_admin')
                            <div class="col-2">
                                <label for="user_id"><span class="text-danger"></span>{{__('User')}}</label>
                                <select class="form-control m-bot15" name="user_id" id="user_id">
                                    <option disabled selected value>Select an option</option>
                                    @foreach($users as $user)
                                        <option
                                            value="{{$user->id}}" {{''}}>{{$user->name}}  </option>
                                    @endforeach
                                </select>
                            </div>
                            @endrole
                            <div class="col-2">
                                <label for="finish"><span class="text-danger"></span>{{__('Status')}}</label>
                                <select class="form-control m-bot15" name="finish" id="finish" data-value="">
                                    <option disabled selected value>Select an option</option>
                                    <option value="1" {{''}}>{{'Finished'}}  </option>
                                    <option value="0" {{''}}>{{'Un Finished'}}  </option>
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="name"><span class="text-danger"></span>{{__('Search')}}</label>
                                <input class="form-control" id="name" name="name" type="text" placeholder="Search..."
                                       value="">
                            </div>
                            <div class="col-2">
                                <button type="submit" onclick="disableNameSearch()" class="btn btn-primary mt-4">{{ __('Search') }}</button>
                            </div>
                        </div>
                        <script>
                                function disableNameSearch()
                                {
                                    let value = $('#name').val();
                                    if (!value)
                                    {
                                        $('#name').remove()
                                    }

                                }
                        </script>
                    </form>

                    <div class="table-responsive py-4">
                        <table class="table align-items-center table-flush" id="datatable-basic">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('#') }}</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Creation Date') }}</th>
                                <th scope="col">{{ __('Dead Line') }}</th>
                                <th scope="col">{{ __('Categories') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>{{ $loop->index + 1}}</td>
                                    <td>{{ $task->name }}</td>
                                    <td>{{ $task->create_date }}</td>
                                    <td>{{ $task->dead_line }}</td>
                                    <td>
                                        @foreach($task->categories as $category)
                                            <span class="badge badge-default"
                                                  style="background-color:{{ $category->color }}">{{ $category->name }}</span>

                                        @endforeach
                                    </td>
                                    <td>
                                        @if($task->finish === 1)
                                            <span class="badge badge-pill badge-lg badge-success">Finished</span>
                                        @else
                                            <span class="badge badge-pill badge-lg badge-warning">On Progress</span>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                               @role('super_admin') <a class="dropdown-item"
                                                   href="{{ route($item.'.edit', $task) }}">{{ __('Edit') }}</a>
                                                @endrole
                                                <form action="{{ route($item.'.destroy', $task) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="button" class="dropdown-item"
                                                            onclick="confirm('{{ __("Are you sure you want to delete this$item. ?") }}') ? this.parentElement.submit() : ''">
                                                        {{ __('Delete') }}
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation ">
                            <ul class="pagination justify-content-center">
                                {!! $tasks->links() !!}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
@endpush
