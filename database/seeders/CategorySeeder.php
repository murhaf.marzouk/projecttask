<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Category::create(['color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
                'name' => 'category' . $i]);
        }
    }
}
