<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case "POST":
            case 'PUT':
                return [
                    'name' => ['required','string','max:255'],
                    'description' => ['required','string','max:1000'],
                    'create_date' => ['required','date'],
                    'dead_line' => ['required','date_format:Y-m-d','after_or_equal:create_date'],
                    'user_id' => ['required',Rule::exists('users','id')],
                    'categories' => ['array'],
                    'categories.*' => ['required',Rule::exists('categories','id')],
                ];
        }
    }
}
