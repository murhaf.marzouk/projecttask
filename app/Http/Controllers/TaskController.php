<?php

namespace App\Http\Controllers;

use App\Filter\TaskFilters;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\TaskRequest;
use App\Models\Category;
use App\Models\Task;
use App\Repositories\CategoryRepository;
use App\Repositories\TaskRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    private $taskRepository, $categoryRepository, $userRepository;
    private $resource = 'task';

    public function __construct(TaskRepository $taskRepository
        , CategoryRepository                   $categoryRepository,
                                UserRepository $userRepository)
    {
        view()->share('item', $this->resource);
        $this->taskRepository = $taskRepository;
        $this->categoryRepository = $categoryRepository;
        $this->userRepository = $userRepository;
        $this->middleware('permission:read_tasks')->only('index') ;
        $this->middleware('permission:create_tasks')->only('create','store') ;
        $this->middleware('permission:update_tasks')->only('edit','update') ;
        $this->middleware('permission:delete_tasks')->only('destroy') ;
    }

    public function index(Request $request)
    {
        $query = Task::query();
        if ($request->get('name'))
        {
            $query->where('name','like','%'.$request->get('name').'%');
        } if ($request->get('finish'))
        {
            $query->where('finish','=',$request->get('finish'));
        }if ($request->get('user_id'))
        {
            $query->where('user_id','=',$request->get('user_id'));
        }
        $tasks = $query->paginate(10)->appends($request->all());
        $users = $this->userRepository->getAll();
        $categories = $this->categoryRepository->getAll();
        return view('task.index', compact('tasks','users','categories'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->getAll();
        $users = $this->userRepository->getAll();
        return view($this->resource . '.create', compact('categories', 'users'));
    }

    public function store(TaskRequest $request): RedirectResponse
    {
        $this->taskRepository->create($request->all());

        $request->session()->flash('success', 'Task created successfully');

        return redirect()->route($this->resource . '.index');
    }

    public function update(TaskRequest $request, Task $task): RedirectResponse
    {
        $this->taskRepository->update($task->id, $request->all());

        $request->session()->flash('success', 'Task updated successfully');


        return redirect()->route($this->resource . '.index');
    }

    public function edit(Task $task)
    {
        $categories = $this->categoryRepository->getAll();
        $users = $this->userRepository->getAll();
        return view($this->resource . '.edit', compact('task', 'categories', 'users'));
    }

    public function destroy(Task $task): RedirectResponse
    {
        $this->taskRepository->delete($task->id);

        return redirect()->route($this->resource . '.index');
    }
}
