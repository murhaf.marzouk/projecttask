<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    private $categoryRepository;
    private $resource = 'category';
    public function __construct(CategoryRepository $categoryRepository)
    {
        view()->share('item', $this->resource);
        $this->categoryRepository = $categoryRepository;

        $this->middleware('role:super_admin');
    }

    public function index(Request $request)
    {

//        $categories = Category::query()->paginate(10 ? $request->size : 10);
        $categories = $this->categoryRepository->index();
        return view('category.index', compact('categories'));
    }

    public function create()
    {
        return view($this->resource.'.create');
    }

    public function store(CategoryRequest $request): RedirectResponse
    {
        $this->categoryRepository->create($request->all());

        $request->session()->flash('success', 'Category created successfully');

        return redirect()->route($this->resource.'.index');
    }

    public function update(CategoryRequest $request, Category $category): RedirectResponse
    {
        $this->categoryRepository->update($category->id, $request->all());

        $request->session()->flash('success', 'Category updated successfully');


        return redirect()->route($this->resource.'.index');
    }

    public function edit(Category $category)
    {
        return view($this->resource.'.edit', compact('category'));
    }


    public function destroy(Category $category): RedirectResponse
    {
        $this->categoryRepository->delete($category->id);

        return redirect()->route($this->resource.'.index');
    }
}
