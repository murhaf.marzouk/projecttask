<?php

namespace App\Models;

use App\Filter\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','create_date','dead_line','finish','user_id'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'create_date' => 'datetime',
        'dead_line' => 'date'
    ];
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class,'task_categories');
    }

    public function subTasks(): HasMany
    {
        return $this->hasMany(SubTask::class,'task_id');
    }

    public function scopeFilter($query, QueryFilter $filter): Builder
    {
        return $filter->apply($query);
    }
    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        self::addGlobalScope('task',function($builder){
            if (Auth::check())
            {
                if(Auth::user()->hasRole('user')){
                    $builder->where('user_id',Auth::id());
                }
            }

        });

    }
}
