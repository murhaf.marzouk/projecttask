<?php


namespace App\Filter;

class TaskFilters extends QueryFilter
{

    public function order($order = "desc")
    {
        return $this->builder->orderBy('id', $order);
    }

    public function take($num)
    {
        return $this->builder->take($num);
    }

    public function finish($value)
    {
        return $this->builder->where('finish','=', $value);
    }

    public function user_id($value)
    {
        return $this->builder->where('user_id', $value);
    }

}
