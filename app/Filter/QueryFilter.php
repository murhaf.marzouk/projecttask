<?php


namespace App\Filter;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class QueryFilter
{

    protected $request;

    protected $builder;

    /**
     * QueryFilter constructor.
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param  Builder  $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {

        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {

            if (method_exists($this, $name)) {

                call_user_func_array([$this, $name], array_filter([$value]));

            }

        }

        return $this->builder;
    }

    //Natasha
    public function filters()
    {
        return $this->request->all();
    }

}
