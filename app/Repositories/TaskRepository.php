<?php

namespace App\Repositories;

use App\Models\Task;

class TaskRepository extends Repository
{
    protected $model, $subTaskRepository;

    public function __construct(Task $model, SubTaskRepository $subTaskRepository)
    {
        $this->model = $model;
        $this->subTaskRepository = $subTaskRepository;
    }

    public function fillter($attributes)
    {
    }

    public function create(array $attributes)
    {
        $task = parent::create($attributes);
        //store Categories
        if (array_key_exists('categories', $attributes)) {
            $task->categories()->attach($attributes['categories']);
        }
        //store SubTasks
        if (array_key_exists('subTasks', $attributes)) {
            $this->subTaskRepository->store($attributes['subTasks'], $task->id);
        }
    }

    public function update($id, array $attributes)
    {
        $task = $this->getById($id);
        parent::update($id, $attributes);
        //update categories for task
        if (array_key_exists('categories', $attributes)) {
            $task->categories()->sync($attributes['categories']);
        }
        //update subTasks
        if (array_key_exists('subTask', $attributes)) {
            $task->subTasks()->delete();
            $this->subTaskRepository->store($attributes['subTask'], $task->id);
        }
        //store new subTasks
        if (array_key_exists('subTasks', $attributes)) {
            $this->subTaskRepository->store($attributes['subTasks'], $task->id);
        }
    }
}
