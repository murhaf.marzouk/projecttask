<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes)
    {
        return parent::create(array_merge($attributes, [
            'password' => Hash::make($attributes['password'])
        ]));
    }

    public function update($id, array $attributes)
    {
        return parent::update($id, array_merge($attributes, [
            'password' => Hash::make($attributes['password'])
        ]));
    }
}
