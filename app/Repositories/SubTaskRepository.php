<?php

namespace App\Repositories;

use App\Models\SubTask;

class SubTaskRepository extends Repository
{
    protected $model;

    public function __construct(SubTask $model)
    {
        $this->model = $model;
    }

    public function store($attributes, $task)
    {
        foreach ($attributes as $attribute) {
            parent::create(['content' => $attribute, 'task_id' => $task]);
        }
    }
}
