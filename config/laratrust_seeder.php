<?php

return [
    'role_structure' => [
        'super_admin' => [
            'tasks' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
        ],
        'user' => [
            'tasks' => 'r,d'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
